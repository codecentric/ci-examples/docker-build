# docker-build

Diese Pipeline demostriert das bauen eines Docker containers mit Project-"Kaniko". Außerdem wird das fertige Image dann in die GitLab Container Registry gepushed.

Mehr Infromationen zu GitLab und Kaniko:
- https://github.com/GoogleContainerTools/kaniko
- https://docs.gitlab.com/ee/ci/docker/using_kaniko.html
